#include <WiFiManager.h>
#include <WiFi.h>
#include <HTTPClient.h>
#include <WiFiClient.h>

#include "ACS712.h"
#include <Filters.h>

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>

#define AP_NAME "ESP32-IOT"
#define AP_PASS "12345678"

#define SCREEN_WIDTH 128 // OLED display width, in pixels
#define SCREEN_HEIGHT 64 // OLED display height, in pixels
// Declaration for SSD1306 display connected using software SPI (default case):
#define OLED_MOSI   23
#define OLED_CLK   18
#define OLED_DC    16
#define OLED_CS    5
#define OLED_RESET 17
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, OLED_MOSI, OLED_CLK, OLED_DC, OLED_RESET, OLED_CS);

ACS712  ACS(A0, 3.3, 4095, 185);

float testFrequency = 50;                     // signal frequency (Hz)
float windowLength = 40.0 / testFrequency;   // how long to average the signal, for statistist
int Sensor = 0;
float intercept = -0.02;  // adjust untuk kalibrasi
float slope = 0.0033;   // adjust untuk kalibrasi
float current_Volts;

String serverName = "http://192.168.172.188/iot-monitoring-arus/api/insert";

unsigned long printPeriod = 5000;     //Refresh rate
unsigned long previousMillis = 0;

void setup() {
  Serial.begin(115200);
  while (!Serial);
  if (!display.begin(SSD1306_SWITCHCAPVCC)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;); // Don't proceed, loop forever
  }

  display.display();
  delay(1000);

  WiFiManager wm;

  bool res;
  res = wm.autoConnect(AP_NAME, AP_PASS); // password protected ap

  if (!res) {
    Serial.println("Failed to connect");
    ESP.restart();
  }
  else {
    //if you get here you have connected to the WiFi
    Serial.println("connected...yeey :)");

  }

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  oleds(0, 15, "Wifi Has Connected");
  display.display();
  delay(1000);

  display.clearDisplay();
  display.setTextSize(1);
  display.setTextColor(WHITE);
  oleds(15, 30, "Please Wait...");
  display.display();
  delay(1000);

  ACS.autoMidPoint();
  Serial.print("MidPoint: ");
  Serial.print(ACS.getMidPoint());
  Serial.print(". Noise mV: ");
  Serial.println(ACS.getNoisemV());


}

void loop() {
  RunningStatistics inputStats;
  inputStats.setWindowSecs( windowLength );

  while ( true ) {
    Sensor = analogRead(39);                // read the analog in value:
    inputStats.input(Sensor);                   // log to Stats function

    if ((unsigned long)(millis() - previousMillis) >= printPeriod) {
      previousMillis = millis();                // update time every second

      current_Volts = intercept + slope * inputStats.sigma(); //Calibartions for offset and amplitude
      current_Volts = current_Volts * (40.3231);             //Further calibrations for the amplitude

      int mA = ACS.mA_AC();
      float I = mA / 1000.0;
      float P = 220.0 * I;

      display.clearDisplay();
      display.setTextSize(1);
      display.setTextColor(WHITE);
      oleds(0, 0, WiFi.localIP().toString());
      oleds(0, 20, String("Volt :") + current_Volts);
      oleds(0, 35, String("Arus :") + I);
      oleds(0, 50, String("Daya :") + P);
      display.display();
      SendToWebService(current_Volts, I, P);
      //      Serial.println(String("V = ") + current_Volts + " V");
      //      Serial.println(String("mA = ") + mA + " mA");
      //      Serial.println(String("I = ") + I + " A");
      //      Serial.println(String("P = ") + P + " Watts");


    }
  }

}

void oleds(int x, int y, String msg) {

  display.setCursor(x, y);
  display.println(msg);

}

void SendToWebService(float volt, float arus, float daya) {
  //  if ((millis() - lastTime) > timerDelay) {
  //Check WiFi connection status
  if (WiFi.status() == WL_CONNECTED) {
    WiFiClient clients;
    HTTPClient http;
    String serverPath = serverName + "?volt=" + (String) volt + "&arus=" + (String) arus + "&daya=" + (String) daya;
    Serial.println(serverPath);
    Serial.print("[HTTP] begin...\n");
    if (http.begin(clients, serverPath)) {  // HTTP


      Serial.print("[HTTP] GET...\n");
      // start connection and send HTTP header
      int httpCode = http.GET();

      // httpCode will be negative on error
      if (httpCode > 0) {
        // HTTP header has been send and Server response header has been handled
        Serial.printf("[HTTP] GET... code: %d\n", httpCode);

        // file found at server
        if (httpCode == HTTP_CODE_OK || httpCode == HTTP_CODE_MOVED_PERMANENTLY) {
          String payload = http.getString();
          Serial.println(payload);
        }
      } else {
        Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
      }

      http.end();
    } else {
      Serial.printf("[HTTP} Unable to connect\n");
    }


  }

  //  }
}
