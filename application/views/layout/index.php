<?php $url =base_url('assets/')?>
<!doctype html>
<html class="no-js" lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Dashboard v.2.0 | Adminpro - Admin Template</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
        ============================================ -->
        <link rel="shortcut icon" type="image/x-icon" href="<?=$url?>img/favicon.ico">
    <!-- Google Fonts
        ============================================ -->
        <link href="<?=$url?>https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800" rel="stylesheet">
    <!-- Bootstrap CSS
        ============================================ -->
        <link rel="stylesheet" href="<?=$url?>css/bootstrap.min.css">
    <!-- Bootstrap CSS
        ============================================ -->
        <link rel="stylesheet" href="<?=$url?>css/font-awesome.min.css">
    <!-- adminpro icon CSS
        ============================================ -->
        <link rel="stylesheet" href="<?=$url?>css/adminpro-custon-icon.css">
    <!-- meanmenu icon CSS
        ============================================ -->
        <link rel="stylesheet" href="<?=$url?>css/meanmenu.min.css">
    <!-- mCustomScrollbar CSS
        ============================================ -->
        <link rel="stylesheet" href="<?=$url?>css/jquery.mCustomScrollbar.min.css">
    <!-- animate CSS
        ============================================ -->
        <link rel="stylesheet" href="<?=$url?>css/animate.css">
    <!-- jvectormap CSS
        ============================================ -->
        <link rel="stylesheet" href="<?=$url?>css/jvectormap/jquery-jvectormap-2.0.3.css">
    <!-- normalize CSS
        ============================================ -->
        <link rel="stylesheet" href="<?=$url?>css/data-table/bootstrap-table.css">
        <link rel="stylesheet" href="<?=$url?>css/data-table/bootstrap-editable.css">
    <!-- normalize CSS
        ============================================ -->
        <link rel="stylesheet" href="<?=$url?>css/normalize.css">
    <!-- charts CSS
        ============================================ -->
        <link rel="stylesheet" href="<?=$url?>css/c3.min.css">
    <!-- style CSS
        ============================================ -->
        <link rel="stylesheet" href="<?=$url?>style.css">
    <!-- responsive CSS
        ============================================ -->
        <link rel="stylesheet" href="<?=$url?>css/responsive.css">
        <link rel="stylesheet" href="https://cdn.datatables.net/1.13.2/css/dataTables.bootstrap.min.css">

    <!-- modernizr JS
        ============================================ -->
        <script src="<?=$url?>js/vendor/modernizr-2.8.3.min.js"></script>
    <!-- jquery
        ============================================ -->
        <script src="<?=$url?>js/vendor/jquery-1.11.3.min.js"></script>
    </head>

    <body>

        <div class="header-top-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <div class="admin-logo">
                            <a href="#"><img src="<?=$url?>img/favicon.png" alt="" width="50"/></a>
                        </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-0 col-xs-12">
                        <div class="header-top-menu"></div>
                    </div>
                    <div class="col-lg-4 col-md-9 col-sm-6 col-xs-12">
                        <div class="header-right-info">
                            <ul class="nav navbar-nav mai-top-nav header-right-menu">
                                <li class="nav-item">
                                    <button>logout</button>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-menu-area">
            <div class="container">

            </div>
        </div>

        <div class="income-order-visit-user-area mg-t-40">
            <div class="container">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="row">
                            <div class="col-lg-4">
                                <div class="income-dashone-total shadow-reset nt-mg-b-30">
                                    <div class="income-title">
                                        <div class="main-income-head">
                                            <h2>Volt</h2>
                                        </div>
                                    </div>
                                    <div class="income-dashone-pro">
                                        <div class="income-rate-total">
                                            <div class="price-adminpro-rate">
                                                <h3 id="volt"></h3>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="income-dashone-total shadow-reset nt-mg-b-30">
                                    <div class="income-title">
                                        <div class="main-income-head">
                                            <h2>Arus</h2>
                                        </div>
                                    </div>
                                    <div class="income-dashone-pro">
                                        <div class="income-rate-total">
                                            <div class="price-adminpro-rate">
                                                <h3 id="arus"></h3>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <div class="income-dashone-total shadow-reset nt-mg-b-30">
                                    <div class="income-title">
                                        <div class="main-income-head">
                                            <h2>Watts</h2>
                                        </div>
                                    </div>
                                    <div class="income-dashone-pro">
                                        <div class="income-rate-total">
                                            <div class="price-adminpro-rate">
                                                <h3 id="daya"> </h3>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="income-dashone-total shadow-reset nt-mg-b-30">
                                    <div class="income-title">
                                        <div class="main-income-head">
                                            <h2>Kwh</h2>
                                        </div>
                                    </div>
                                    <div class="income-dashone-pro">
                                        <div class="income-rate-total">
                                            <div class="price-adminpro-rate">
                                                <h3 id="kwh"></h3>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="income-dashone-total shadow-reset nt-mg-b-30">
                                    <div class="income-title">
                                        <div class="main-income-head">
                                            <h2>Rupiah</h2>
                                        </div>
                                    </div>
                                    <div class="income-dashone-pro">
                                        <div class="income-rate-total">
                                            <div class="price-adminpro-rate">
                                                <h3 id="rupiah"></h3>
                                            </div>
                                        </div>
                                        <div class="clear"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="sparkline9-list shadow-reset">
                            <div class="sparkline9-hd">
                                <div class="main-sparkline9-hd">
                                    <h1>Setting</h1>
                                    <div class="sparkline9-outline-icon">
                                        <span class="sparkline9-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                        <span id="edit"><i class="fa fa-wrench"></i></span>
                                        <span class="sparkline9-collapse-close"><i class="fa fa-times"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="sparkline9-graph dashone-comment">
                                <form id="form-configs">
                                    <input type="hidden" name="id_" id="id_" value="">
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-4 col-form-label">Golongan</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="gol_tarif" class="form-control" id="golongan" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPassword3" class="col-sm-4 col-form-label">Daya</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="daya" class="form-control" id="daya_va" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputPassword3" class="col-sm-4 col-form-label">Rupiah Kwh</label>
                                        <div class="col-sm-8">
                                            <input type="text" name="rupiah_kwh" class="form-control" id="rupiah_kwh" disabled="">
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-sm-8">
                                            <button type="submit" hidden class="btn btn-primary update">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="feed-mesage-project-area">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12">
                        <div class="sparkline9-list shadow-reset mg-tb-30">
                            <div class="sparkline9-hd">
                                <div class="main-sparkline9-hd">
                                    <h1>Tabel Data Series</h1>
                                    <div class="sparkline9-outline-icon">
                                        <span class="sparkline9-collapse-link"><i class="fa fa-chevron-up"></i></span>
                                        <span><i class="fa fa-wrench"></i></span>
                                        <span class="sparkline9-collapse-close"><i class="fa fa-times"></i></span>
                                    </div>
                                </div>
                            </div>
                            <div class="sparkline9-graph dashone-comment">
                                <table class="table" id="datas">
                                    <thead>
                                        <tr>
                                            <th>Time</th>
                                            <th>Volt</th>
                                            <th>Arus</th>
                                            <th>Daya</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                        </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="footer-copyright-area">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="footer-copy-right">
                            <p>Copyright &#169; 2018 Colorlib All rights reserved. Template by <a href="<?=$url?>https://colorlib.com">Colorlib</a>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Chat Box End-->

    <!-- bootstrap JS
        ============================================ -->
        <script src="<?=$url?>js/bootstrap.min.js"></script>
    <!-- meanmenu JS
        ============================================ -->
        <script src="<?=$url?>js/jquery.meanmenu.js"></script>
    <!-- mCustomScrollbar JS
        ============================================ -->
        <script src="<?=$url?>js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sticky JS
        ============================================ -->
        <script src="<?=$url?>js/jquery.sticky.js"></script>
    <!-- scrollUp JS
        ============================================ -->
        <script src="<?=$url?>js/jquery.scrollUp.min.js"></script>
    <!-- scrollUp JS
        ============================================ -->
        <script src="<?=$url?>js/wow/wow.min.js"></script>
    <!-- counterup JS
        ============================================ -->
        <script src="<?=$url?>js/counterup/jquery.counterup.min.js"></script>
        <script src="<?=$url?>js/counterup/waypoints.min.js"></script>
        <!-- <script src="<?=$url?>js/counterup/counterup-active.js"></script> -->
    <!-- peity JS
        ============================================ -->
        <script src="<?=$url?>js/peity/jquery.peity.min.js"></script>
        <!-- <script src="<?=$url?>js/peity/peity-active.js"></script> -->

    <!-- data table JS
        ============================================ -->
        <script src="https://cdn.datatables.net/1.13.2/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.13.2/js/dataTables.bootstrap.min.js"></script>
    <!-- main JS
        ============================================ -->
        <script src="<?=$url?>js/main.js"></script>

        <script>
            const base_url= '<?=base_url()?>';
            let table;
            $(document).ready(function () {
                $('#edit').on('click',function(argument) {
                    $('form input').removeAttr('disabled');
                    $('.update').removeAttr('hidden')
                });

                $('form#form-configs').submit(function(e) {
                    e.preventDefault();
                    e.stopImmediatePropagation();

                    let FormData = $(this).serialize();
                    $.ajax({
                        url: base_url+'api/updateconfigs',
                        type: 'post',
                        data: FormData,
                        success: function (data) {
                            $('form input').attr('disabled',true);
                            $('.update').attr('hidden',true)
                            GetConfig()
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                });
                table = $('#datas').DataTable({
                    "dom": 'rtip',
                    "scrollY": "250px",
                    "scrollCollapse": true,
                    "processing": false,
                    "paging": false,
                    order: [[0, 'desc']],
                    ajax: {
                        url: base_url + "api/getdata",
                        type: "POST",
                        dataSrc: "",
                    },
                    columns: [{
                        data: "created_at"
                    },
                    {
                        data: "volt"
                    },
                    {
                        data: "arus"
                    },
                    {
                        data: "daya"
                    },

                    ],

                });
                let rupiaha=0;
                const durasi = 60 / 60;
                let arrKwh=[];
                let lastId = 0;
                let kwh =0;
                let ttl=0;
                let lastime=0;
                let delaytime = 60;
                let seconds = 0;
                let minutes = 0;
                GetLastRecord()
                function GetLastRecord() {
                    console.log(rupiaha);
                    $.ajax({
                        url: base_url+'api/getdatalast',
                        type: 'get',
                        async:true,
                        success: function (data) {
                            lastime +=1000;
                            seconds =lastime/1000;
                            if(data.success){

                                kwh += parseFloat(data.data.daya).toFixed(2) / 1000;
                                ttl += rupiaha * kwh.toFixed(4);
                            }else{
                                kwh = parseFloat(data.data.daya).toFixed(2) / 1000;
                                ttl = rupiaha * kwh.toFixed(4);
                            }
                            $('#volt').html(parseFloat(data.data.volt) +' V');
                            $('#arus').html(parseFloat(data.data.arus) + ' A');
                            $('#daya').html(parseFloat(data.data.daya) + ' W');
                            $('#kwh').html(parseFloat(kwh.toFixed(4)) + ' Kwh');
                            $('#rupiah').html('Rp '+ttl.toFixed(2));
                            updateData(data.data.id_monitoring);
                            
                        }
                    });

                }

                function sumArray(array) {
                    let sum = 0;


                    for (const item of array) {
                        sum += item;
                    }

                    // console.log(sum);
                    return sum;
                }

                function updateData(sid) {
                    $.ajax({
                        url: base_url+'api/updaterecord',
                        type: 'get',
                        data: {sid:sid},
                        success: function (data) {
                            // console.log(data);
                        }
                    });
                }
                GetConfig();
                function GetConfig() {
                    $.ajax({
                        url: base_url+'api/configs',
                        type: 'get',
                        async:false,
                        success: function (data) {
                            $('#id_').val(data.data.id_config);
                            $('#golongan').val(data.data.gol_tarif);
                            $('#daya_va').val(data.data.daya);
                            $('#rupiah_kwh').val(data.data.rupiah_kwh);
                            rupiaha = data.data.rupiah_kwh;
                        },
                        error: function(xhr, status, error) {
                            console.log(xhr.responseText);
                        }
                    });
                }

                setInterval(function() {
                    table.ajax.reload();
                    GetLastRecord()
                    
                }, 1000);

                function calculate() {
                    console.log(seconds);
                    if(seconds == delaytime){

                        // console.log(seconds);
                        minutes += 1;
                        lastime = 0;
                        seconds = 0;
                        console.log('menit :'+minutes+' detik : '+seconds);
                    }

                }
            });
        </script>
    </body>

    </html>