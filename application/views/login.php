<?php $url = base_url('assets/') ?>
<!doctype html>
<html class="no-js" lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<title>Login | IOT-MONRUS</title>
	<meta name="description" content="">
	<meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- favicon
    	============================================ -->
    	<link rel="shortcut icon" type="image/x-icon" href="<?=$url?>img/favicon.png">
    <!-- Google Fonts
    	============================================ -->
    	<link href="<?=$url?>https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i,800" rel="stylesheet">
    <!-- Bootstrap CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?=$url?>css/bootstrap.min.css">
    <!-- Bootstrap CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?=$url?>css/font-awesome.min.css">
    <!-- adminpro icon CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?=$url?>css/adminpro-custon-icon.css">
    <!-- meanmenu icon CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?=$url?>css/meanmenu.min.css">
    <!-- mCustomScrollbar CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?=$url?>css/jquery.mCustomScrollbar.min.css">
    <!-- animate CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?=$url?>css/animate.css">
    <!-- normalize CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?=$url?>css/normalize.css">
    <!-- form CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?=$url?>css/form.css">
    <!-- style CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?=$url?>style.css">
    <!-- responsive CSS
    	============================================ -->
    	<link rel="stylesheet" href="<?=$url?>css/responsive.css">
    <!-- modernizr JS
    	============================================ -->
    	<script src="<?=$url?>js/vendor/modernizr-2.8.3.min.js"></script>
    </head>

    <body>
    <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="<?=$url?>http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
        	<!-- Header top area start-->
        	
        	<!-- Header top area end-->
        	<!-- Main Menu area start-->
        	<!-- Main Menu area End-->
        	<!-- Mobile Menu start -->
        	<div class="mobile-menu-area">
        		<div class="container">
        			<div class="row">
        				<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        					<div class="mobile-menu">
        						<nav id="dropdown">
        							<ul class="mobile-menu-nav">
        								<li><a data-toggle="collapse" data-target="#Charts" href="#">Home <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
        									<ul class="collapse dropdown-header-top">
        										<li><a href="<?=$url?>dashboard.html">Dashboard v.1</a>
        										</li>
        										<li><a href="<?=$url?>dashboard-2.html">Dashboard v.2</a>
        										</li>
        										<li><a href="<?=$url?>analytics.html">Analytics</a>
        										</li>
        										<li><a href="<?=$url?>widgets.html">Widgets</a>
        										</li>
        									</ul>
        								</li>
        								<li><a data-toggle="collapse" data-target="#demo" href="#">Mailbox <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
        									<ul id="demo" class="collapse dropdown-header-top">
        										<li><a href="<?=$url?>inbox.html">Inbox</a>
        										</li>
        										<li><a href="<?=$url?>view-mail.html">View Mail</a>
        										</li>
        										<li><a href="<?=$url?>compose-mail.html">Compose Mail</a>
        										</li>
        									</ul>
        								</li>
        								<li><a data-toggle="collapse" data-target="#others" href="#">Miscellaneous <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
        									<ul id="others" class="collapse dropdown-header-top">
        										<li><a href="<?=$url?>profile.html">Profile</a>
        										</li>
        										<li><a href="<?=$url?>contact-client.html">Contact Client</a>
        										</li>
        										<li><a href="<?=$url?>contact-client-v.1.html">Contact Client v.1</a>
        										</li>
        										<li><a href="<?=$url?>project-list.html">Project List</a>
        										</li>
        										<li><a href="<?=$url?>project-details.html">Project Details</a>
        										</li>
        									</ul>
        								</li>
        								<li><a data-toggle="collapse" data-target="#Miscellaneousmob" href="#">Interface <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
        									<ul id="Miscellaneousmob" class="collapse dropdown-header-top">
        										<li><a href="<?=$url?>google-map.html">Google Map</a>
        										</li>
        										<li><a href="<?=$url?>data-maps.html">Data Maps</a>
        										</li>
        										<li><a href="<?=$url?>pdf-viewer.html">Pdf Viewer</a>
        										</li>
        										<li><a href="<?=$url?>x-editable.html">X-Editable</a>
        										</li>
        										<li><a href="<?=$url?>code-editor.html">Code Editor</a>
        										</li>
        										<li><a href="<?=$url?>tree-view.html">Tree View</a>
        										</li>
        										<li><a href="<?=$url?>preloader.html">Preloader</a>
        										</li>
        										<li><a href="<?=$url?>images-cropper.html">Images Cropper</a>
        										</li>
        									</ul>
        								</li>
        								<li><a data-toggle="collapse" data-target="#Chartsmob" href="#">Charts <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
        									<ul id="Chartsmob" class="collapse dropdown-header-top">
        										<li><a href="<?=$url?>bar-charts.html">Bar Charts</a>
        										</li>
        										<li><a href="<?=$url?>line-charts.html">Line Charts</a>
        										</li>
        										<li><a href="<?=$url?>area-charts.html">Area Charts</a>
        										</li>
        										<li><a href="<?=$url?>rounded-chart.html">Rounded Charts</a>
        										</li>
        										<li><a href="<?=$url?>c3.html">C3 Charts</a>
        										</li>
        										<li><a href="<?=$url?>sparkline.html">Sparkline Charts</a>
        										</li>
        										<li><a href="<?=$url?>peity.html">Peity Charts</a>
        										</li>
        									</ul>
        								</li>
        								<li><a data-toggle="collapse" data-target="#Tablesmob" href="#">Tables <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
        									<ul id="Tablesmob" class="collapse dropdown-header-top">
        										<li><a href="<?=$url?>static-table.html">Static Table</a>
        										</li>
        										<li><a href="<?=$url?>data-table.html">Data Table</a>
        										</li>
        									</ul>
        								</li>
        								<li><a data-toggle="collapse" data-target="#Tablesmob" href="#">Forms <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
        									<ul id="Tablesmob" class="collapse dropdown-header-top">
        										<li><a href="<?=$url?>basic-form-element.html">Basic Form Elements</a>
        										</li>
        										<li><a href="<?=$url?>advance-form-element.html">Advanced Form Elements</a>
        										</li>
        										<li><a href="<?=$url?>password-meter.html">Password Meter</a>
        										</li>
        										<li><a href="<?=$url?>multi-upload.html">Multi Upload</a>
        										</li>
        										<li><a href="<?=$url?>tinymc.html">Text Editor</a>
        										</li>
        										<li><a href="<?=$url?>dual-list-box.html">Dual List Box</a>
        										</li>
        									</ul>
        								</li>
        								<li><a data-toggle="collapse" data-target="#Appviewsmob" href="#">App views <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
        									<ul id="Appviewsmob" class="collapse dropdown-header-top">
        										<li><a href="<?=$url?>basic-form-element.html">Basic Form Elements</a>
        										</li>
        										<li><a href="<?=$url?>advance-form-element.html">Advanced Form Elements</a>
        										</li>
        										<li><a href="<?=$url?>password-meter.html">Password Meter</a>
        										</li>
        										<li><a href="<?=$url?>multi-upload.html">Multi Upload</a>
        										</li>
        										<li><a href="<?=$url?>tinymc.html">Text Editor</a>
        										</li>
        										<li><a href="<?=$url?>dual-list-box.html">Dual List Box</a>
        										</li>
        									</ul>
        								</li>
        								<li><a data-toggle="collapse" data-target="#Pagemob" href="#">Pages <span class="admin-project-icon adminpro-icon adminpro-down-arrow"></span></a>
        									<ul id="Pagemob" class="collapse dropdown-header-top">
        										<li><a href="<?=$url?>login.html">Login</a>
        										</li>
        										<li><a href="<?=$url?>register.html">Register</a>
        										</li>
        										<li><a href="<?=$url?>captcha.html">Captcha</a>
        										</li>
        										<li><a href="<?=$url?>checkout.html">Checkout</a>
        										</li>
        										<li><a href="<?=$url?>contact.html">Contacts</a>
        										</li>
        										<li><a href="<?=$url?>review.html">Review</a>
        										</li>
        										<li><a href="<?=$url?>order.html">Order</a>
        										</li>
        										<li><a href="<?=$url?>comment.html">Comment</a>
        										</li>
        									</ul>
        								</li>
        							</ul>
        						</nav>
        					</div>
        				</div>
        			</div>
        		</div>
        	</div>
        	<!-- Mobile Menu end -->
        	<!-- Breadcome start-->

        	<div class="login-form-area mg-t-30 mg-b-40">
        		<div class="container">
        			<div class="row">
        				<div class="col-lg-3"></div>
        				<form id="adminpro-form" class="adminpro-form" method="post" action="<?=base_url('login/actlogin')?>">
        					<div class="col-lg-6">
        						<div class="login-bg">
        							<div class="row">
        								<div class="col-lg-12">
        									<div class="logo">
        										<a href="#"><img src="<?=$url?>img/favicon.png" alt="" width="60"/> 
        											<span class="text-uppercase" style="font-size: 35px;font-weight: bold;"> IOT-MONRUS</span>
        										</a>
        									</div>
        								</div>
        							</div>
        							<div class="row">
        								<div class="col-lg-12">
        									<div class="login-title">
        										<h1>Login Form</h1>
        									</div>
        								</div>
        							</div>
        							<div class="row">
        								<div class="col-lg-4">
        									<div class="login-input-head">
        										<p>E-mail</p>
        									</div>
        								</div>
        								<div class="col-lg-8">
        									<div class="login-input-area">
        										<input type="text" name="username" />
        										<i class="fa fa-user login-user" aria-hidden="true"></i>
        									</div>
        								</div>
        							</div>
        							<div class="row">
        								<div class="col-lg-4">
        									<div class="login-input-head">
        										<p>Password</p>
        									</div>
        								</div>
        								<div class="col-lg-8">
        									<div class="login-input-area">
        										<input type="password" name="password" />
        										<i class="fa fa-lock login-user"></i>
        									</div>

        								</div>
        							</div>
        							<div class="row">
        								<div class="col-lg-4">

        								</div>
        								<div class="col-lg-8">
        									<div class="login-button-pro">
        										<button type="submit" class="login-button login-button-lg btn-block">Log in</button>
        									</div>
        								</div>
        							</div>
        						</div>
        					</div>
        				</form>
        				<div class="col-lg-3"></div>
        			</div>
        		</div>
        	</div>
        	
        	<!-- Footer End-->
    <!-- jquery
    	============================================ -->
    	<script src="<?=$url?>js/vendor/jquery-1.11.3.min.js"></script>
    <!-- bootstrap JS
    	============================================ -->
    	<script src="<?=$url?>js/bootstrap.min.js"></script>
    <!-- meanmenu JS
    	============================================ -->
    	<script src="<?=$url?>js/jquery.meanmenu.js"></script>
    <!-- mCustomScrollbar JS
    	============================================ -->
    	<script src="<?=$url?>js/jquery.mCustomScrollbar.concat.min.js"></script>
    <!-- sticky JS
    	============================================ -->
    	<script src="<?=$url?>js/jquery.sticky.js"></script>
    <!-- scrollUp JS
    	============================================ -->
    	<script src="<?=$url?>js/jquery.scrollUp.min.js"></script>
    <!-- form validate JS
    	============================================ -->
    	<script src="<?=$url?>js/jquery.form.min.js"></script>
    	<script src="<?=$url?>js/jquery.validate.min.js"></script>
    <!-- main JS
    	============================================ -->
    	<script src="<?=$url?>js/main.js"></script>
    </body>

    </html>