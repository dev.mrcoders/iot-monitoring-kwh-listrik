<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

	public function insert()
	{
		$get = $this->input->get();

		$get['created_at']=date('Y-m-d H:i:s');

		$this->db->insert('tb_data_monitoring', $get);

		$this->output->set_content_type('application/json')->set_output(json_encode($get));
	}

	public function UpdateRecord()
	{

		$this->db->where('id_monitoring', $this->input->get('sid'));
		$this->db->update('tb_data_monitoring', ['record_stats'=>1]);
		$this->output->set_content_type('application/json')->set_output(json_encode($get));
	}

	public function GetData()
	{
		$this->db->order_by('id_monitoring', 'desc');
		$Data = $this->db->get('tb_data_monitoring')->result();

		$this->output->set_content_type('application/json')->set_output(json_encode($Data));
	}

	public function GetDataLast()
	{
		$this->db->order_by('id_monitoring', 'desc');
		$this->db->limit(1);
		$Data = $this->db->get('tb_data_monitoring')->row();

		if($Data->record_stats == 0){
			$Respons =[
				'success'=>true,
				'message'=>"ok",
				'data'=>$Data
			];
		}else{
			$Respons =[
				'success'=>false,
				'message'=>"no",
				'data'=>$Data
			];
		}

		$this->output->set_content_type('application/json')->set_output(json_encode($Respons));
	}

	public function configs()
	{
		$this->db->where('aktif', 'Y');
		$Data = $this->db->get('tb_config')->row();

		$Respons =[
			'success'=>true,
			'message'=>"no",
			'data'=>$Data
		];
		$this->output->set_content_type('application/json')->set_output(json_encode($Respons));

	}

	public function updateConfigs()
	{
		$Post=$this->input->post();
		unset($Post['id_']);
		$this->db->where('id_config', $this->input->post('id_'));
		$this->db->update('tb_config', $Post);
		$Respons =[
			'success'=>true,
			'message'=>"no",
			'data'=>$Post
		];
		$this->output->set_content_type('application/json')->set_output(json_encode($Respons));
	}

}

/* End of file Api.php */
/* Location: ./application/controllers/Api.php */