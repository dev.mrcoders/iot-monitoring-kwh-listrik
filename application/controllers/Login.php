<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('M_login');
	}

	public function index()
	{
		$this->load->view('login');
		
	}

	public function ActLogin()
	{

		$post=$this->input->post();
		$post['password']=md5($post['password']);

		$CheckingLogin = $this->M_login->Getuser($post);

		if($CheckingLogin){
			redirect(base_url().'welcome','refresh');
		}else{
			redirect(base_url());
		}
		

	}
}
